Google Cloud Storage System

Copyright (c) 2014 Long Nangong, Jiada Tu, Zhe Miao  
All right reserved.

How to run:

1. Website:
a) Open http://basic-bank-746.appspot.com/
b) The functions are pretty clear, so just follow what you see
c) Remember, you can only download/delete/check one file at a time
d) Some file (larger than 30MB) can’t be download from website side. When you “list”, it will tell you if a file can be download from web browser
e) Now, the server has no file left. So you will not see any file if you try to list now.

2. Client side:
a) Open eclipse. 
b) import GCSClient project
c) add external jars from directory “”(long)
d) Run
e) the program will tell you how to do next (right?long)

3. generate file
a) Run “python genFiles.py [directory]” 
*[directory] is the directory path that you want to save all 411 files.
*if no [directory] is passed, it will be default as “./files”
b) wait for several minutes
c) done

How to deploy:
1. Server side (the web application/ servlets)
a) Open eclipse. Here we assume your eclipse has install the “gae plugin for eclipse”
b) import GoogleCloudStorageSystem project
c) add external jars from directory “”(long)
d) use “gae plugin for eclipse” to deploy it to app engine