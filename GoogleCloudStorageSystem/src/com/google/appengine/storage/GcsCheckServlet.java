package com.google.appengine.storage;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;

//This class is used to respond the request to 
public class GcsCheckServlet extends HttpServlet {
	private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
    .initialRetryDelayMillis(10)
    .retryMaxAttempts(10)
    .totalRetryPeriodMillis(15000)
    .build());
	
	private static final String BUCKETNAME = "cs553-a3-bucket";
	private static final int CACHE_SIZE = 100 * 1024; //size = 100KB
	GcsMemcache cache = new GcsMemcache();
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException,IOException {
		
		
		try{
			int ret,ret_mem,ret_gcs;
			String filename = req.getParameter("filename");
			String fullname;
			String resp_str = new String();
			
			//find just the file with the filename itself, in case it's not big enough to be split into slices
			ret_gcs = CheckFile(filename);
			
			int n=0;
			if(ret_gcs == 0)
			{
				//find all the slices -- filename_part_*
				fullname = filename+"_part_";
				for(int i=1;;i++)
				{
					ret_mem = CheckFile(fullname+Integer.toString(i));
					
					if(ret_mem == 0)
					{
						n = i;
						break;
					}
					
				}
			}
			
			if(ret_gcs != 0){
				resp_str += filename+ " " + evalError(ret_gcs);
			}else if(n > 2){
				resp_str += filename+ " " + evalError(1);
			}
				
			resp.setContentType("text/plain");
			resp.getOutputStream().write(resp_str.getBytes());
			
		}catch(Exception ex){
			throw new ServletException(ex);
		}
		
		
	}

	/*
	 * return: 0 -- not found anywhere
	 * 		   1 -- found in gcs
	 *		   2 -- found in both gcs and mem
	 * 		
	 */
	
	public int CheckFile(String filename) throws IOException{
		boolean ifCached = true;
		
		try{
			int ret;
			
			if(filename.isEmpty())
			{
				return ret=0;
			}
			
			GcsFilename file = new GcsFilename(BUCKETNAME,filename);
			
			if(gcsService.getMetadata(file) != null)
			{
				ret = 1; 
				
				if(ifCached && cache.contains(filename))
				{
					ret = 2;
				}
			}else{
				ret = 0;
			}
			
			return ret;
		}catch(Exception ex){
			throw new IOException(ex);
		}
	}
	
	public String evalError(int err)
	{
		String ret = new String();
		switch(err)
		{
		case 0:
			ret = "Not Found Anywhere!\n";
			break;
		case 1:
			ret = " File found only in GCS\n";
			break;
		case 2:
			ret = " File found both in cache\n";
			break;
		}
		
		return ret;
	}
}