package com.google.appengine.storage;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import net.sf.jsr107cache.Cache;
import net.sf.jsr107cache.CacheException;
import net.sf.jsr107cache.CacheFactory;
import net.sf.jsr107cache.CacheManager;
import net.sf.jsr107cache.CacheStatistics;

import com.google.appengine.api.memcache.MemcacheService;
import com.google.appengine.api.memcache.MemcacheService.SetPolicy;
import com.google.appengine.api.memcache.jsr107cache.GCacheFactory;

/****MemCache Utilities*****/
public class GcsMemcache {
	Cache cache;
    
	public GcsMemcache() {
        try {
            CacheFactory cacheFactory = CacheManager.getInstance().getCacheFactory();
//          Map props = createPolicy();
//          cache = cacheFactory.createCache(props);
            cache = cacheFactory.createCache(Collections.emptyMap());       
        } catch (CacheException e) {
            e.printStackTrace();
        }
    }
 
    private Map createPolicy() {
    	Map<SetPolicy, Boolean> props = new HashMap<SetPolicy, Boolean>();
    	props.put(MemcacheService.SetPolicy.SET_ALWAYS, true);
        return props;
    }
    
    public void put(String key, byte[] data) {
        cache.put(key, data);
    }
 
    public byte[] get(String key) {
        return (byte[]) cache.get(key);
    }
    
    public boolean contains(String key) {
    	return cache.containsKey(key);
    }
 
    public String fetchCacheStatistics() {
        CacheStatistics stats = cache.getCacheStatistics();
        int hits = stats.getCacheHits();
        int misses = stats.getCacheMisses();
        return "Cache Hits=" + hits + " : Cache Misses=" + misses;
    }
    
    //added by Mike
    public boolean delete(String key){
    	boolean ret = false;
    	
    	cache.remove(key);
  
    	if(!cache.containsKey(key))
    		ret = true;
    	return ret;
    	
    }
    
}
