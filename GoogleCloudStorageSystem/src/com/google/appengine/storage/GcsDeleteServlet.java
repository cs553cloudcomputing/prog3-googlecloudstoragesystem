package com.google.appengine.storage;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;
import com.google.appengine.tools.cloudstorage.GcsFilename;


public class GcsDeleteServlet extends HttpServlet {
	private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
    .initialRetryDelayMillis(10)
    .retryMaxAttempts(10)
    .totalRetryPeriodMillis(15000)
    .build());
	
	private static final String BUCKETNAME = "cs553-a3-bucket";
	private static final int CACHE_SIZE = 100 * 1024; //size = 100KB
	GcsMemcache cache = new GcsMemcache();
	
	private static int ret;
	
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) 
			throws ServletException,IOException {
		try{
			String filename = req.getParameter("filename");
			String fullname;
			String resp_str;
			
			//find just the file with the filename itself, in case it's not big enough to be split into slices
			ret = DeleteFile(filename);
			if(ret == 1){
				//find all the slices -- filename_part_*
				fullname = filename+"_part_";
				int rev;
				for(int i=1;;i++)
				{
					rev = DeleteFile(fullname+Integer.toString(i));
					if(rev == 0){
						ret = rev;
					}
					if(rev == 1)
					{
						break;
					}
					
				}
			}
			
			resp_str = filename+ " " + evalError(ret);

			resp.setContentType("text/plain");
			resp.getOutputStream().write(resp_str.getBytes());
			
		
		}catch(Exception ex){
			throw new ServletException(ex);
		}
	}
	
	/*
	 * return: 0 -- succeed
	 * 		   1 -- file not found in gcs nor cache
	 * 		   2 -- file not found in cache
	 * 	       3 -- delete failure
	 */
	public int DeleteFile(String filename) throws IOException{
		try{
			int ret = 0;
			GcsFilename file = new GcsFilename(BUCKETNAME,filename);					
//			int fileSize = (int) gcsService.getMetadata(file).getLength();//get file size
			
			//delete file in gcs
			if(!gcsService.delete(file))
			{
				return ret = 1;
			}
			
			//search for the file in cache if the file size is smaller than CACHE_SIZE			
			if(cache.contains(filename)){
				cache.delete(filename);	//remove file in the memcache			
			}
			
			return ret;
		}catch(Exception ex){
			throw new IOException(ex);
		}
	}
	
	public String evalError(int err)
	{
		String ret = new String();
		switch(err)
		{
		case 0:
			ret = "Delete done!\n";
			break;
		case 1:
			ret = "File not found in GCS\n";
			break;
		case 2:
			ret = "File not found in cache\n";
			break;
		case 3:
			ret = "Delete failure\n";
			break;
		}
		
		return ret;
	}
	
	
	
	

}
