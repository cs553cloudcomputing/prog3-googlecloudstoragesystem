package com.google.appengine.storage;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
//import java.io.ObjectInputStream;
//import java.io.ObjectOutputStream;
//import java.util.Arrays;











import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

import com.google.appengine.tools.cloudstorage.GcsFileMetadata;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.ListItem;
import com.google.appengine.tools.cloudstorage.ListOptions;
import com.google.appengine.tools.cloudstorage.ListResult;
import com.google.appengine.tools.cloudstorage.RetryParams;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class FilesInfoServlet extends HttpServlet {
	
	private static final String BUCKETNAME = "cs553-a3-bucket";
 final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
      .initialRetryDelayMillis(10)
      .retryMaxAttempts(10)
      .totalRetryPeriodMillis(15000)
      .build());
 
 	static class fs {
 		public String fn;
 		public long size;
 		public long startPoint;
 		public Integer index;
 		public fs(String fn, long size, int index) {
 			this.fn=fn;
 			this.size=size;
 			this.index=index;
 		}
 	}


	@Override
	  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException {
		  try {
			  DataInputStream in = new DataInputStream(req.getInputStream());
			  DataOutputStream out = new DataOutputStream(resp.getOutputStream());
			  String fn = null;
			  try {
				  while((fn=in.readUTF()) != null) {
					  String fnsplit = fn+"_part_";
					  ListOptions.Builder lb = new ListOptions.Builder();
					  Iterator<ListItem> lr = gcsService.list(BUCKETNAME,lb.setPrefix(fn).setRecursive(true).build());
					  boolean ifFind = false;
					  boolean ifSplit = true;
					  ArrayList<fs> af = new ArrayList<fs>();
					  long totalSize = 0;
					  while(lr.hasNext()) {
						  ListItem li=lr.next();
						  System.out.println(li.getName());
						  if(li.getName().equals(fn)) {
							  ifFind = true;
							  ifSplit = false;
							  totalSize = li.getLength();
							  break;
						  }
						  else if(li.getName().startsWith(fnsplit)) {
							  boolean ifIs = true;
							  int index=0;
							  try{
								  index=Integer.parseInt(li.getName().substring(fnsplit.length()));
							  } catch(Exception ex) {
								  ifIs=false;
							  }
							  if(index==0)	ifIs=false;
							  if(!ifIs)	continue;
							  ifFind = true;
							  ifSplit = true;
							  af.add(new fs(li.getName(),li.getLength(),index));
						  }
					  }
					  out.writeUTF(fn);
					  out.writeBoolean(ifFind);
					  if(!ifFind)	continue;
					  out.writeBoolean(ifSplit);
					  if(!ifSplit) {
						  out.writeLong(totalSize);
						  continue;
					  }
					  totalSize = 0;
					  Collections.sort(af, new Comparator<fs>() {
					        @Override
					        public int compare(fs  fs1, fs  fs2)
					        {
						           return  fs1.index.compareTo(fs2.index);
						    }
					  });
					  for(int i=0;i<af.size();i++) {
						  af.get(i).startPoint=totalSize;
						  totalSize+=af.get(i).size;
					  }
					  out.writeLong(totalSize);
					  out.writeInt(af.size());
					  for(int i=0;i<af.size();i++) {
						  out.writeUTF(af.get(i).fn);
						  out.writeLong(af.get(i).startPoint);
						  out.writeInt(af.get(i).index);
					  }
				  }
			  } catch (EOFException e) { ;}
			  
		  } catch (Exception ex) {
		      throw new ServletException(ex);
		  }
	  }

	
}
