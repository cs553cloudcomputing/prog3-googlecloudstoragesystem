package com.google.appengine.storage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.appengine.tools.cloudstorage.GcsFileMetadata;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/*******************************
 * The Get method of GCS System.*
 *******************************/
public class GcsGetServlet extends HttpServlet {
	
//	public static final boolean SERVE_USING_BLOBSTORE_API = false;
	private static final String BUCKETNAME = "cs553-a3-bucket";
	private static final int CACHE_SIZE = 100 * 1024; //size = 100KB
  /**
   * This is the service from which all requests are initiated,
   * and where backoff parameters are configured. Here it is aggressively retrying with
   * backoff, up to 10 times but taking no more that 15 seconds total to do so.
   */
	private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
      .initialRetryDelayMillis(10)
      .retryMaxAttempts(10)
      .totalRetryPeriodMillis(15000)
      .build());

	/***Configure the cache instance***/
	GcsMemcache cache = new GcsMemcache();
	   
	/**Used below to determine the size of chucks to read in. Should be > 1kb and < 10MB */
	private static final int BUFFER_SIZE = 5 * 1024 * 1024;
  	
	/**Transfer the data from the inputStream to the outputStream. Then close both streams.*/
	private void copy(InputStream input, OutputStream output) throws IOException {
		try {
		    byte[] buffer = new byte[BUFFER_SIZE];
		    int bytesRead = input.read(buffer);
		    while (bytesRead != -1) {
		    	output.write(buffer, 0, bytesRead);
		        bytesRead = input.read(buffer);
		    }
	    } finally {
	      input.close();
	      output.close();
	    }
	}
	
	  /**
	   * Retrieves a file from GCS and returns it in the http response.
	   * If the request path is /gcs/Foo/Bar this will be interpreted as
	   * a request to read the GCS file named Bar in the bucket Foo.
	   */
	//find()
	@Override
	  public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException {
		Boolean ifCached = true;
		
		try {
			  String fp = req.getParameter("filepath");
			  if (fp == null || fp == "") {
			      resp.setContentType("text/plain");
			      resp.getOutputStream().write("File name can not be null or empty!".getBytes());
			      return;
			  }
			  String [] lst = fp.split("/");
			  lst = lst[lst.length-1].split("\\\\");
			  String fn = lst[lst.length-1];
			  System.out.println("filename="+fn);
			  System.out.println("filepath="+fp);
			  String mimeType = null;
			  InputStream in = null;
			  
			  byte [] content = cache.get(fp);
			  if (ifCached && content != null) {
				  mimeType = "application/octet-stream";
				  in = new ByteArrayInputStream(content);
			  }
			  else {
				  GcsFilename gcsFN = new GcsFilename(BUCKETNAME, fp);
				  GcsFileMetadata meta = gcsService.getMetadata(gcsFN);
				  if (meta == null) {
				      resp.setContentType("text/plain");
				      resp.getOutputStream().write("No such file exist!".getBytes());
//					  System.out.println(fp+" not exist");
					  return;
				  }
				  mimeType = meta.getOptions().getMimeType();
				  if (mimeType == null) {
					  System.out.println("mime type = null");
					  mimeType = "application/octet-stream";
				  }
				  GcsInputChannel readChannel = gcsService.openPrefetchingReadChannel(gcsFN, 0, 1024 * 1024);
				  in = Channels.newInputStream(readChannel);
				  
				  if (meta.getLength() <= CACHE_SIZE) {
					  ByteArrayOutputStream bOut = new ByteArrayOutputStream();
					  copy(in,bOut);
					  byte[] bytes = bOut.toByteArray();
					  in = new ByteArrayInputStream(bytes);
					  if (!cache.contains(fp))
						  cache.put(fp, bytes);
				  }
			  }
			  System.out.println("mime type="+ mimeType);
			  resp.setContentType(mimeType);
			  resp.setHeader("Content-Disposition", "attachment; filename=\"" + fn + "\"");
			  OutputStream out = resp.getOutputStream();
			  copy(in,out);
			  
		  } catch (Exception ex) {
		      throw new ServletException(ex);
		  }
	  }

}
