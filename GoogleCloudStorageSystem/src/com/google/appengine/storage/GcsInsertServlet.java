package com.google.appengine.storage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;

import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.appengine.tools.cloudstorage.GcsFileMetadata;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/***********************************
 * The Insert method of GCS System.*
 ***********************************/
public class GcsInsertServlet extends HttpServlet {
	
//	public static final boolean SERVE_USING_BLOBSTORE_API = false;
	private static final String BUCKETNAME = "cs553-a3-bucket";
	private static final int CACHE_SIZE = 100 * 1024; //size = 100KB
  /**
   * This is the service from which all requests are initiated,
   * and where backoff parameters are configured. Here it is aggressively retrying with
   * backoff, up to 10 times but taking no more that 15 seconds total to do so.
   */
	private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
      .initialRetryDelayMillis(10)
      .retryMaxAttempts(10)
      .totalRetryPeriodMillis(15000)
      .build());

	/***Configure the cache instance***/
	GcsMemcache cache = new GcsMemcache();
	   
	/**Used below to determine the size of chucks to read in. Should be > 1kb and < 10MB */
	private static final int BUFFER_SIZE = 5 * 1024 * 1024;
  	
	/**Transfer the data from the inputStream to the outputStream. Then close both streams.*/
	private void copy(InputStream input, OutputStream output) throws IOException {
		try {
		    byte[] buffer = new byte[BUFFER_SIZE];
		    int bytesRead = input.read(buffer);
		    while (bytesRead != -1) {
		    	output.write(buffer, 0, bytesRead);
		        bytesRead = input.read(buffer);
		    }
	    } finally {
	      input.close();
	      output.close();
	    }
	}
	
	
	/**
	 * Writes the payload of the incoming post as the contents of a file to GCS.
	 * If the request path is /gcs/Foo/Bar this will be interpreted as
	 * a request to create a GCS file named Bar in bucket Foo.
	 */
	//insert()
	@Override
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException {
		boolean ifCached = true;
		
		boolean isMultipart = ServletFileUpload.isMultipartContent(req); //Check if it is actually a file upload request.
		
		if(isMultipart){
			try {
				ServletFileUpload upload = new ServletFileUpload(); //File upload handler
	
				FileItemIterator iterator = upload.getItemIterator(req);
			
				while (iterator.hasNext()) {
					FileItemStream item = iterator.next();
					InputStream uploadedStream = item.openStream(); //input file stream
		
					//check if have partitions for identical file name 
					String filename = item.getName();
					GcsFilename file = new GcsFilename(BUCKETNAME,filename+"_part_1");
					if(gcsService.getMetadata(file) != null){
						for(int i=1; ;i++)
						{
							if(!DeletePartition(filename+"_part_"+i)){ break; }
						}
					}
					
					//Continue to store new file to GCS 
					GcsFilename fileName = new GcsFilename(BUCKETNAME,item.getName()); //get gcs filename
					GcsOutputChannel outputChannel =
						  gcsService.createOrReplace(fileName, GcsFileOptions.getDefaultInstance());//open gcs output channel
				  
					copy(uploadedStream, Channels.newOutputStream(outputChannel)); //write uploaded file to gcs
					
					if(ifCached){
						int fileSize = (int) gcsService.getMetadata(fileName).getLength();//get file size from gcs service
	
						if(fileSize <= CACHE_SIZE){
							ByteBuffer data = ByteBuffer.allocate(fileSize);
							try (GcsInputChannel readChannel = gcsService.openReadChannel(fileName, 0)) {
							      readChannel.read(data);
							}				
							/******Cache file to the Memcache ****/
							cache.put(item.getName(), data.array());
							
						}
					}
				}
		      
				// respond to query
		      	resp.setContentType("text/plain");
		      	resp.getOutputStream().write("File uploaded successfully!".getBytes());
		      
			} catch (Exception ex) {
				throw new ServletException(ex);
			}
		}
		
	}
	
	public boolean DeletePartition(String filename) throws IOException{
		try{
			GcsFilename file = new GcsFilename(BUCKETNAME,filename);					
	
			//delete file in gcs
			if(!gcsService.delete(file))
			{
				return false;
			}
			
			//search for the file in the cache		
			if(cache.contains(filename)){
				cache.delete(filename);	//remove file in the memcache			
			}
	
			return true;
		}catch(Exception ex){
			throw new IOException(ex);
		}
	}
	
}
