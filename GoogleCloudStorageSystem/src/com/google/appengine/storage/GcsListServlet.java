package com.google.appengine.storage;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.google.appengine.tools.cloudstorage.GcsFileMetadata;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.ListItem;
import com.google.appengine.tools.cloudstorage.ListOptions;
import com.google.appengine.tools.cloudstorage.RetryParams;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class GcsListServlet extends HttpServlet {
    
    //	public static final boolean SERVE_USING_BLOBSTORE_API = false;
    private static final String BUCKETNAME = "cs553-a3-bucket";
    private static final int CACHE_SIZE = 100 * 1024; //size = 100KB
    
    private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder()
                                                                             .initialRetryDelayMillis(10)
                                                                             .retryMaxAttempts(10)
                                                                             .totalRetryPeriodMillis(15000)
                                                                             .build());
    
    GcsMemcache cache = new GcsMemcache();
	   
    
    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,IOException {
        try {
            ArrayList<String> lfns = new ArrayList<String>();
            Iterator<ListItem> lr = gcsService.list(BUCKETNAME,ListOptions.DEFAULT);
            String s;
            String str="";
            while(lr.hasNext()) {
                ListItem li=lr.next();
                s = li.getName();
                if(s.matches(".+_part_\\d+")) {
                    s = s.substring(0,s.lastIndexOf("_part_"));
                    if(lfns.contains(s))
                        continue;
                }
                lfns.add(s);
                if(cache.contains(s))
                    str = str+"Filename: "+"\""+s+"\"\r\nLocation: memcache";
                else
                    str = str+"Filename: "+"\""+s+"\"\r\nLocation: GCS";
                if(s.equals(li.getName()))
                    str = str+"\r\nIf can be downloaded by web-browser(>32MB): True\r\n\r\n";
                else
                    str = str+"\r\nIf can be downloaded by web-browser(>32MB): False\r\n\r\n";
            }
            str = str+"\r\n";
            resp.setContentType("text/plain; charset=utf-8");
            resp.getOutputStream().write(str.getBytes("UTF-8"));
        } catch (Exception ex) {
            throw new ServletException(ex);
        }
    }
    
}
