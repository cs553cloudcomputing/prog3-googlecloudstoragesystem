<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>

<!DOCTYPE html>

<html>
<head>
	<title>Google Cloud Storage</title>
	<link type="text/css" rel="stylesheet" href="/css/main.css"/>		
</head>

<h1>Welcome to Google Cloud Storage!</h1>

<body>

<%

	UserService userService = UserServiceFactory.getUserService();
	User user = userService.getCurrentUser();
	if (user != null) {
        pageContext.setAttribute("user", user);
	}
    
%>


<h3>File Upload:</h3>
Select file(s) to upload <br />
<br />
<form name="uploadform" action="gcs/insert" method="post" enctype="multipart/form-data">
<input type="file" name="filename" size="50" multiple/><br />
<br />
<input type="submit" value="Upload File" />
</form>


<h3>File Check:</h3>
Enter the file name to check<br /><br />
<form name="checkform" action="gcs/check" method="post">
<input type="text" name="filename" size="50" /><br /><br />
<input type="submit" value="Check File" />
</form>

<h3>File List:</h3>
<form name="listform" action="gcs/list" method="post">
<input type="submit" value="List Files" />
</form>

<h3>File Download:</h3>
Enter the absolute file path to download<br /><br />
<form name="downloadform" action="gcs/get" method="post">
<input type="text" name="filepath" size="50" /><br /><br />
<input type="submit" value="Download File" />
</form>


<!-- added by Mike -->
<h3>File Delete:</h3>
Enter the file name to delete<br /><br />
<form name="deleteform" action="gcs/delete" method="post">
<input type="text" name="filename" size="50" /><br /><br />
<input type="submit" value="Delete File" />
</form>
<!-- end -->
 


</body>
</html>