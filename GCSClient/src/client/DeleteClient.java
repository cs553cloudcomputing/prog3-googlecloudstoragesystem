package client;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;


//Multithreaded Client Delete
public class DeleteClient {

	private final static int MAX_SIZE = 30 * 1024 * 1024; //size = 30MB	 
	private static int NUM_THREADS;
	
	public DeleteClient(int NUM_THREADS){
		DeleteClient.NUM_THREADS = NUM_THREADS;
	}
	
	public void delete() throws Exception{
		List<File> FileList = new ArrayList<File>();
    	
    	//workload pre-processing
    	final File folder = new File("/home/ryan/Desktop/workloads");  	
    	for (final File fileEntry : folder.listFiles()) {                  
    		FileList.add(fileEntry);
    	}
		
		PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
    	connManager.setDefaultMaxPerRoute(4);
    	connManager.setMaxTotal(4);
    	
    	CloseableHttpClient httpclient = HttpClients.custom().
    		    setConnectionManager(connManager).build();
    	
    	//Start clock
    	long start_time = System.nanoTime();
        // create thread(s) for each file delete
        DeletetThread[] threads = new DeletetThread[NUM_THREADS];
        for (int i = 0; i < threads.length; i++) {
        	   threads[i] = new DeletetThread(httpclient, FileList, i);
               threads[i].start();// start the thread
        }    
        
        // wait the threads to finish
        for (int j = 0; j < threads.length; j++) {
            threads[j].join();
        }
  
        //Stop clock
        long end_time = System.nanoTime();
        double elapsed_time = (end_time - start_time)/1e6;
        System.out.println("Time to delete file(s) = "+elapsed_time+" millisecond.");
        
        httpclient.close();
    	   	
	}
	
	 static class DeletetThread extends Thread {
		 HttpClient httpclient;
    	 List<File> FileList;
    	 int id;
    	 
    	 HttpPost httppost = new HttpPost("http://basic-bank-746.appspot.com/gcs/delete");
    	 //HttpPost httppost = new HttpPost("http://localhost:8888/gcs/delete");
    	 
    	 public DeletetThread(HttpClient httpclient, List<File> FileList,int id) {
    		 this.httpclient = httpclient;
    		 this.FileList = FileList;
    		 this.id = id;
    	 }
    	 
    	 public void run() {
    		try{
    			for(int index = id; index < FileList.size(); index+=NUM_THREADS){
	    			File file = FileList.get(index);
	    		
	    			 BasicNameValuePair pair = new BasicNameValuePair("filename", file.getName());
	    			
	    			 ArrayList<NameValuePair> nameValuePair = new ArrayList<NameValuePair>();
	                
	    			 nameValuePair.add(pair);
		            
		             httppost.setEntity(new UrlEncodedFormEntity(nameValuePair));
		            
		             //System.out.println("Executing request: " + httppost.getRequestLine());
		             HttpResponse response = httpclient.execute(httppost);
		          
		             //System.out.println("----------------------------------------");
		             //System.out.println(response.getStatusLine());
		             try {
		                 //System.out.println("----------------------------------------");
		                 //System.out.println(response.getStatusLine());
		                 EntityUtils.consume(response.getEntity());
		             } catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					 }
	      
    			}
	         } catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			 } catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
			 }
    		
    	 }
    	  	 
    }

}
