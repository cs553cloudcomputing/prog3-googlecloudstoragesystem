package client;


import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;



public class HttpFilesDownloadThread implements Runnable {
	CloseableHttpClient httpclient ;
	private static final int BUFFER_SIZE = 5 * 1024 * 1024;
	String url;
	ArrayList<FileInfo> fis;
	ArrayList<String> dsts;
	
	public HttpFilesDownloadThread(String url, ArrayList<FileInfo> fis, ArrayList<String> dsts) {
		assert(fis.size() == dsts.size());
		this.httpclient = HttpClients.createDefault();
		this.url=url;
		this.fis=fis;
		this.dsts=dsts;
	}
	
	private void copy(InputStream input, OutputStream output) throws IOException {
		try {
		    byte[] buffer = new byte[BUFFER_SIZE];
		    int bytesRead;
		    while ((bytesRead = input.read(buffer)) != -1) {
		    	output.write(buffer, 0, bytesRead);
		    }
	    } finally {;}
	}
	
	public void get(FileInfo fi, String dst) throws ClientProtocolException, IOException {
		if(!fi.ifSplited) {
			List<NameValuePair> nvplst = new ArrayList<NameValuePair>();
			HttpPost request = new HttpPost(url);
			String fn=fi.name;
			nvplst.add(new BasicNameValuePair("filepath",fn));
			HttpEntity entity = EntityBuilder.create().setParameters(nvplst).build();
			request.setEntity(entity);
			CloseableHttpResponse response = httpclient.execute(request);
			if (response.getStatusLine().getStatusCode() != 200) {
				System.out.println("Error happens when download file: File \"" + fn + "\" not found!");
				return;
			}
			InputStream in = response.getEntity().getContent();
	        OutputStream out = new FileOutputStream(dst);
	        copy(in,out);
	        in.close();
	        out.close();
	        response.close();
		}
		else {
	        OutputStream out = null;
	        boolean ifOpen = false;
			for(int i=0;i<fi.slices.size();i++) {
				HttpPost request = new HttpPost(url);
				List<NameValuePair> nvplst = new ArrayList<NameValuePair>();
				String fn=fi.slices.get(i);
				nvplst.add(new BasicNameValuePair("filepath",fn));
				HttpEntity entity = EntityBuilder.create().setParameters(nvplst).build();
				request.setEntity(entity);
				CloseableHttpResponse response = httpclient.execute(request);
				if (response.getStatusLine().getStatusCode() != 200) {
					System.out.println("Error happens when download file: File \"" + fn + "\" not found!");
					continue;
				}
				InputStream in = response.getEntity().getContent();
				if(!ifOpen) {
					out = new FileOutputStream(dst);
					ifOpen = true;
				}
		        copy(in,out);
		        in.close();
		        response.close();
			}
			if(ifOpen)
				out.close();
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			for(int i=0;i<fis.size();i++)
				get(fis.get(i),dsts.get(i));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
