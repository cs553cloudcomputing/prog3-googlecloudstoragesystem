package client;

import java.util.ArrayList;

public class FileInfo {
	public String name;
	public long size;
	public boolean ifSplited;
	public ArrayList<String> slices;
	
	public FileInfo(String name, long size, boolean ifSplited, ArrayList<String> slices) {
		if(ifSplited)
			assert(slices!=null);
		this.name=name;
		this.size=size;
		this.ifSplited=ifSplited;
		this.slices=slices;
	}
}
