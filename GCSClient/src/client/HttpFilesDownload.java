package client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

public class HttpFilesDownload {
	int numOfThread;
	String downloadUrl;
	String filesInfoUrl;
	List<String> fns;
	List<String> dsts;
	ArrayList<FileInfo> afi;
	public HttpFilesDownload(int numOfThread, String downloadUrl, String filesInfoUrl, List<String> fns, List<String> dsts) {
		assert(numOfThread>0);
		assert(fns!=null && dsts!=null);
		assert(fns.size()==dsts.size());
		this.numOfThread=numOfThread;
		this.fns=fns;
		this.dsts=dsts;
		this.downloadUrl=downloadUrl;
		this.filesInfoUrl=filesInfoUrl;
	}
	
	public void getFilesInfo() throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream dos = new DataOutputStream(baos);
		for(int i=0; i<fns.size(); i++) {
			dos.writeUTF(fns.get(i));
		}
		dos.close();
		baos.close();
		byte[] bytes = baos.toByteArray();
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost request = new HttpPost(filesInfoUrl);
		HttpEntity entity = EntityBuilder.create().setBinary(bytes).build();
		request.setEntity(entity);
		CloseableHttpResponse response = httpclient.execute(request);
		DataInputStream dis = new DataInputStream(response.getEntity().getContent());
		this.afi = new ArrayList<FileInfo>();
		for(int i=0;i<fns.size();i++) {
			String fn = dis.readUTF();
			boolean ifFind = dis.readBoolean();
			if(!ifFind) {
				this.afi.add(null);
				continue;
			}
			boolean ifSplit = dis.readBoolean();
			long size = dis.readLong();
			if(!ifSplit) {
				this.afi.add(new FileInfo(fn,size,ifSplit,null));
				continue;
			}
			ArrayList<String> slices = new ArrayList<String>();
			int totalSlices = dis.readInt();
			for(int j=0;j<totalSlices;j++) {
				String str=dis.readUTF();
				slices.add(str);
				dis.readLong();
				dis.readInt();
			}
			this.afi.add(new FileInfo(fn,size,ifSplit,slices));
		}
        response.close();
	}
	
	private int findMin(Long[] a, int size) {
		int index=0;
		for(int i=1;i<size;i++)
			if(a[i]<a[index])
				index=i;
		return index;
	}
	
	public void doStart() throws IOException {
		getFilesInfo();
		ArrayList<HttpFilesDownloadThread> lst = new ArrayList<HttpFilesDownloadThread>();
		ArrayList< ArrayList<FileInfo> > aafi = new ArrayList< ArrayList<FileInfo> >();
		ArrayList< ArrayList<String> > aadst = new ArrayList< ArrayList<String> >();
		Long[] ai = new Long[numOfThread];
		for(int i=0; i<numOfThread; i++) {
			aafi.add(new ArrayList<FileInfo>());
			aadst.add(new ArrayList<String>());
			ai[i]=(long) 0;
		}
		for(int i=0; i<afi.size(); i++) {
			if(afi.get(i)==null) continue;
			int index=findMin(ai,numOfThread);
			aafi.get(index).add(afi.get(i));
			aadst.get(index).add(dsts.get(i));
			ai[index]+=afi.get(i).size;
		}
		List<Thread> lt = new ArrayList<Thread>();
		for(int i=0; i<numOfThread; i++) {
			HttpFilesDownloadThread hfdt = new HttpFilesDownloadThread(downloadUrl, aafi.get(i), aadst.get(i));
			lst.add(hfdt);
			Thread t = new Thread(hfdt);
			lt.add(t);
			t.start();
		}
		try {
			for(Thread thread : lt) {
				thread.join();
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
