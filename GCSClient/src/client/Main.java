package client;

import java.util.Scanner;

public class Main {

	public static void main(String[] args){		
		int choice=-1;
		int n_thread;
		Scanner user_input = new Scanner( System.in );
		
		System.out.print("Input number of THREADS to sending request: ");
		n_thread = user_input.nextInt(); //running threads
		
		while(choice != 0){
			menu();
			choice = user_input.nextInt();
			
			switch(choice){
			case 1: //Upload files
				System.out.println("Uploading....");
				UploadClient uploadclient = new UploadClient(n_thread);
				try {
					uploadclient.upload();
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case 2: //Download files
				System.out.println("Downloading....");
				DownloadClient downloadclient = new DownloadClient(n_thread);
				try {
					downloadclient.download();
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case 3: //Delete files
				System.out.println("Removing....");
				DeleteClient deleteclient = new DeleteClient(n_thread);
				try {
					deleteclient.delete();
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			default:
				break;
			}
		}
		
		
	}
	
	public static void menu(){
		System.out.println("===================Benchmarking Options==================");
		System.out.println("1. Upload dataset(411 files,total size=311MB) to GCS.");
		System.out.println("2. Download dataset(822 files, total size=622MB) from GCS.");
		System.out.println("3. Remove dataset(411 files, total size=311MB) from GCS");
		System.out.print("Choose the benchmarking option(0->exit): ");
	}
}
