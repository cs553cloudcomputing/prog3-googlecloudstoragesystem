package client;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

//Multithreaded Client Upload
public class UploadClient {
    
    private final static int MAX_SIZE = 30 * 1024 * 1024; //size = 30MB 
    private static int NUM_THREADS;
	
	public UploadClient(int NUM_THREADS){
		UploadClient.NUM_THREADS = NUM_THREADS;
	}
	
    public void upload() throws Exception {
    	List<File> FileList = new ArrayList<File>();
    	
    	//workload pre-processing
    	final File folder = new File("/home/ryan/Desktop/workloads");  	
    	for (final File fileEntry : folder.listFiles()) {                  
               if(fileEntry.length() <= MAX_SIZE) {
            	   FileList.add(fileEntry);
               }
               else {
            	   splitfile(FileList, fileEntry);            	   
               }
    	}   	
    	
    	PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager();
    	connManager.setDefaultMaxPerRoute(4);
    	connManager.setMaxTotal(4);
    	
    	CloseableHttpClient httpclient = HttpClients.custom().
    		    setConnectionManager(connManager).build();
    	
    	//Start clock
    	long start_time = System.nanoTime();
        // create thread(s) for each file upload
        InsertThread[] threads = new InsertThread[NUM_THREADS];
        for (int i = 0; i < threads.length; i++) {
            threads[i] = new InsertThread(httpclient, FileList, i);
            threads[i].start();// start the thread
        }          
     
        // wait the threads to finish
        for (int j = 0; j < threads.length; j++) {
            threads[j].join();
        }
  
        //Stop clock
        long end_time = System.nanoTime();
        double elapsed_time = (end_time - start_time)/1e6;
        System.out.println("Time to upload file(s) = "+elapsed_time+" millisecond.");
        
        httpclient.close();
      
    }
    
    static class InsertThread extends Thread {
    	 HttpClient httpclient;
    	 List<File> FileList;
    	 private int id;
    	 HttpPost httppost = new HttpPost("http://basic-bank-746.appspot.com/gcs/insert");
    	 //HttpPost httppost = new HttpPost("http://localhost:8888/gcs/insert");
    	 
    	 public InsertThread(HttpClient httpclient, List<File> FileList, int id) {
    		 this.httpclient = httpclient;
    		 this.FileList = FileList;
    		 this.id = id;
    	 }
    	 
    	 public void run() {
    		try{
    			for(int index = id; index < FileList.size(); index+=NUM_THREADS){
	    			File file = FileList.get(index);
	    			
		    		 HttpEntity httpEntity = MultipartEntityBuilder.create()
		             	    .addBinaryBody("file", file, ContentType.create("binary/octet-stream"), file.getName())
		             	    .build();
		                         
		             httppost.setEntity(httpEntity);
		            
		             //System.out.println("Executing request: " + httppost.getRequestLine());
		             HttpResponse response = httpclient.execute(httppost);
		             try {
		                 //System.out.println("----------------------------------------");
		                 //System.out.println(response.getStatusLine());
		                 EntityUtils.consume(response.getEntity());
		             } catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    			}
	         } catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			 } catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
			 }
    		
    	 }
    	  	 
    }
    
    private static void splitfile(List<File> FileList, File infile) {   	
    	try {  		
    		FileInputStream input = new FileInputStream(infile);
    		
    		int index = 1;
        	String filename = infile.getName()+"_part_"+index;
    		File outfile =new File(filename);
    		FileList.add(outfile); 		
    		FileOutputStream output = new FileOutputStream(outfile);
    		int FILE_SIZE = 0;
    		
    		//Split large file 
    		byte[] buffer = new byte[100];
		    int bytesRead = input.read(buffer);
		    while (bytesRead != -1) {
		    	if(FILE_SIZE < MAX_SIZE){	
		    		output.write(buffer, 0, bytesRead); 
		    		FILE_SIZE += buffer.length;
		    		
		    	}
		    	else{	
		    		output.close();
		    		index++;
		    		FILE_SIZE = 0;
		    		
		    		filename = infile.getName()+"_part_"+index;
		    		outfile =new File(filename);
		    		FileList.add(outfile);	
		    		
		    		output = new FileOutputStream(outfile);		    		
		    		output.write(buffer, 0, bytesRead);
		    		FILE_SIZE += buffer.length;
		    	}
		    	
		        bytesRead = input.read(buffer);
 	
		    }		    
		    input.close();		
		    output.close();
		    
    	} catch (IOException x) {
    	    System.err.println(x);
    	}
    }
    
        
}