package client;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

//Multithreaded Client Download
public class DownloadClient {
	private static int NUM_THREADS;
	
	public DownloadClient(int NUM_THREADS){
		DownloadClient.NUM_THREADS = NUM_THREADS;
	}
	
	public void download() throws Exception{
		try {
			ArrayList<String> fns = new ArrayList<String>();
			ArrayList<String> dsts = new ArrayList<String>();
	    	
			//add the files name that you want to download to the list of download file 
	    	final File folder = new File("/home/ryan/Desktop/workloads");  	
	    	for (final File fileEntry : folder.listFiles()) {                  
	    		fns.add(fileEntry.getName());
	    	}
	    	
			//downloadUrl = the get page
			String downloadUrl = "http://basic-bank-746.appspot.com/gcs/get";
			//String downloadUrl="http://localhost:8888/gcs/get";
			
			//filesInfoUrl = the filesinfo page
			String filesInfoUrl="http://basic-bank-746.appspot.com/filesinfo";
			//String filesInfoUrl="http://localhost:8888/filesinfo";
		
			//add the local paths that you want these files to be saved to dsts
			for(String filename : fns){
				dsts.add("/home/ryan/Desktop/download/"+filename+1);
			}
			long start_time;
			long end_time;
			//Start clock
	    	start_time = System.nanoTime();
			HttpFilesDownload hfd_1 = new HttpFilesDownload(NUM_THREADS,downloadUrl,filesInfoUrl,fns,dsts);
			hfd_1.doStart();
			//Stop clock
	        end_time = System.nanoTime();
	        double elapsed_time_1 = (end_time - start_time)/1e6;
	        
			dsts.clear();
			for(String filename : fns){
				dsts.add("/home/ryan/Desktop/download/"+filename+2);
			}
			//Start clock
	    	start_time = System.nanoTime();
			HttpFilesDownload hfd_2 = new HttpFilesDownload(NUM_THREADS,downloadUrl,filesInfoUrl,fns,dsts);
			hfd_2.doStart();
			//Stop clock
	        end_time = System.nanoTime();
	        double elapsed_time_2 = (end_time - start_time)/1e6;
	        
	        double elapsed_time = elapsed_time_1+elapsed_time_2;
	        System.out.println("Time to download file(s) = "+elapsed_time+" millisecond.");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
