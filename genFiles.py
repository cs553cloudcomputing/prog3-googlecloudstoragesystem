import os
import sys
import string
import random

def genFileName(fnList, size=10, chars=string.ascii_letters+string.digits):
	fn=''.join(random.choice(chars) for _ in range(size))
	while fn in fnList:
		fn=''.join(random.choice(chars) for _ in range(size))
	return fn

def genFile(fn, path, size, lineSize=100, chars=string.ascii_letters+string.digits):
	fp=open(path+fn, 'w')
	lines=int(size/lineSize)
	lastLineSize=size%lineSize
	for i in range(0,lines):
		str=''.join(random.choice(chars) for _ in range(lineSize-2))+'\r\n'
		fp.write(str)
	if lastLineSize!=0:
		str=''.join(random.choice(chars) for _ in range(lastLineSize))
		fp.write(str)
	fp.close()

def genFiles(fnList, path, num, size):
	for i in range(0,num):
		fn=genFileName(fnList)
		genFile(fn, path, size)
		fnList.append(fn)

if len(sys.argv)>1:
    path=sys.argv[1]
else:
    path='./files'
if path[-1]!='/':
	path=path+'/'
if not os.path.exists(path):
	os.makedirs(path)
fnList=[]

genFiles(fnList, path, 100, 1024)
genFiles(fnList, path, 100, 10*1024)
genFiles(fnList, path, 100, 100*1024)
genFiles(fnList, path, 100, 1024*1024)
genFiles(fnList, path, 10, 10*1024*1024)
genFiles(fnList, path, 1, 100*1024*1024)
print fnList
